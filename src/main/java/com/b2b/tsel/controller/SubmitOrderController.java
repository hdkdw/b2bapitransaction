package com.b2b.tsel.controller;

import com.b2b.tsel.dto.ResponseSubmitOrder;
import com.b2b.tsel.dto.StaticResponse;
import com.b2b.tsel.dto.SubmitOrder;
import com.b2b.tsel.dto.TranscationDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
@Slf4j
public class SubmitOrderController {


    @PostMapping("/submitorder")
    public ResponseEntity<?> submitOrder(@RequestBody SubmitOrder submitOrder ){

        TranscationDTO transcationDTO = new TranscationDTO();
        transcationDTO.setTransaction_id(StaticResponse.TRANSACTION_ID);
        transcationDTO.setChannel_transaction_id(submitOrder.getTransaction_id());
        transcationDTO.setChannel(submitOrder.getChannel());
        transcationDTO.setStatus_code(StaticResponse.STATUS_CODE);
        transcationDTO.setStatus_desc(StaticResponse.STATUS_DESC);

        ResponseSubmitOrder responseSubmitOrder = new ResponseSubmitOrder();
        responseSubmitOrder.setTransactions(transcationDTO);
        responseSubmitOrder.setIseligible(StaticResponse.ISELIGIBLE);
        responseSubmitOrder.setService_id_a(submitOrder.getService_id_a());
        responseSubmitOrder.setService_id_b(submitOrder.getService_id_b());
        responseSubmitOrder.setNotification(StaticResponse.NOTIFICATION);

        return ResponseEntity.ok(responseSubmitOrder);

    }

    @GetMapping("/callbak/sms-dlr")
    @ResponseBody
    public ResponseEntity<?> getCallback(@RequestParam("msisdn") String msisdn,
                              @RequestParam("msgid") String msgid,
                              @RequestParam("status") String status,
                              @RequestParam("err") Integer err,
                              @RequestParam("final") Integer finalz){

        if (msisdn != null && msgid != null && status != null && err != null & finalz != null){

            return ResponseEntity.ok().body("SUCCESS");
        }
        else {
            return ResponseEntity.badRequest().body("params not complete");
        }


    }






    
}
