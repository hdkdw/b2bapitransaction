package com.b2b.tsel.dto;

import java.util.List;

public class ResponseSubmitOrder {

        private TranscationDTO transactions;
        private boolean iseligible;
        private String service_id_a;
        private String service_id_b;
        private String notification;

        public ResponseSubmitOrder(){

        }
        
        public ResponseSubmitOrder(TranscationDTO transactions, boolean iseligible, String service_id_a,
                String service_id_b, String notification) {
            this.transactions = transactions;
            this.iseligible = iseligible;
            this.service_id_a = service_id_a;
            this.service_id_b = service_id_b;
            this.notification = notification;
        }
        public TranscationDTO getTransactions() {
            return transactions;
        }
        public void setTransactions(TranscationDTO transactions) {
            this.transactions = transactions;
        }
        public boolean isIseligible() {
            return iseligible;
        }
        public void setIseligible(boolean iseligible) {
            this.iseligible = iseligible;
        }
        public String getService_id_a() {
            return service_id_a;
        }
        public void setService_id_a(String service_id_a) {
            this.service_id_a = service_id_a;
        }
        public String getService_id_b() {
            return service_id_b;
        }
        public void setService_id_b(String service_id_b) {
            this.service_id_b = service_id_b;
        }
        public String getNotification() {
            return notification;
        }
        public void setNotification(String notification) {
            this.notification = notification;
        }


        


      }


