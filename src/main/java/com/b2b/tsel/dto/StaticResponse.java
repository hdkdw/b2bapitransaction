package com.b2b.tsel.dto;

public class StaticResponse {

    public static final String TRANSACTION_ID = "u01798acbcddc08379000236011a40fHac100001000";

    public static final String STATUS_CODE ="0000";
    public static final String STATUS_DESC = "Success";
    public static final boolean ISELIGIBLE = true;
    public static final String NOTIFICATION= "Terima kasih, permintaan anda sedang diproses. Silahkan tunggu SMS selanjutnya.";



}
