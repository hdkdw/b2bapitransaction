package com.b2b.tsel.dto;


public class SubmitOrder {

    private String channel;
    private String alacarte;
    private Integer commitment_period;
    private String  penalty_indicator;
    private Integer penalty_flat_rate;
    private String id;
    private String OCR;
    private boolean scheduler_flag;
    private String order_type;
    private String payment_method;
    private Integer subscription_flag;
    private String transaction_id;
    private String quotation_reference;
    private String bundle_id;
    private String offer_id;
    private Integer parent_offer_id;
    private String parent_trx_id;
    private Integer bulk_id;
    private Integer price;
    private Integer parent_price;
    private Integer penalty_remaining_period;
    private Integer scheduler_scenario;
    private Integer corporate_id;
    private String service_id_a;
    private String service_id_b;
    private String purchase_mode;
    private String callback_url;

    


    public SubmitOrder(String channel, String alacarte, Integer commitment_period, String penalty_indicator,
            Integer penalty_flat_rate, String id, String oCR, boolean scheduler_flag, String order_type,
            String payment_method, Integer subscription_flag, String transaction_id, String quotation_reference,
            String bundle_id, String offer_id, Integer parent_offer_id, String parent_trx_id, Integer bulk_id,
            Integer price, Integer parent_price, Integer penalty_remaining_period, Integer scheduler_scenario,
            Integer corporate_id, String service_id_a, String service_id_b, String purchase_mode, String callback_url) {
        this.channel = channel;
        this.alacarte = alacarte;
        this.commitment_period = commitment_period;
        this.penalty_indicator = penalty_indicator;
        this.penalty_flat_rate = penalty_flat_rate;
        this.id = id;
        OCR = oCR;
        this.scheduler_flag = scheduler_flag;
        this.order_type = order_type;
        this.payment_method = payment_method;
        this.subscription_flag = subscription_flag;
        this.transaction_id = transaction_id;
        this.quotation_reference = quotation_reference;
        this.bundle_id = bundle_id;
        this.offer_id = offer_id;
        this.parent_offer_id = parent_offer_id;
        this.parent_trx_id = parent_trx_id;
        this.bulk_id = bulk_id;
        this.price = price;
        this.parent_price = parent_price;
        this.penalty_remaining_period = penalty_remaining_period;
        this.scheduler_scenario = scheduler_scenario;
        this.corporate_id = corporate_id;
        this.service_id_a = service_id_a;
        this.service_id_b = service_id_b;
        this.purchase_mode = purchase_mode;
        this.callback_url = callback_url;
    }
    public String getChannel() {
        return channel;
    }
    public void setChannel(String channel) {
        this.channel = channel;
    }
    public String getAlacarte() {
        return alacarte;
    }
    public void setAlacarte(String alacarte) {
        this.alacarte = alacarte;
    }
    public Integer getCommitment_period() {
        return commitment_period;
    }
    public void setCommitment_period(Integer commitment_period) {
        this.commitment_period = commitment_period;
    }
    public String getPenalty_indicator() {
        return penalty_indicator;
    }
    public void setPenalty_indicator(String penalty_indicator) {
        this.penalty_indicator = penalty_indicator;
    }
    public Integer getPenalty_flat_rate() {
        return penalty_flat_rate;
    }
    public void setPenalty_flat_rate(Integer penalty_flat_rate) {
        this.penalty_flat_rate = penalty_flat_rate;
    }
    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }
    public String getOCR() {
        return OCR;
    }
    public void setOCR(String oCR) {
        OCR = oCR;
    }
    public boolean isScheduler_flag() {
        return scheduler_flag;
    }
    public void setScheduler_flag(boolean scheduler_flag) {
        this.scheduler_flag = scheduler_flag;
    }
    public String getOrder_type() {
        return order_type;
    }
    public void setOrder_type(String order_type) {
        this.order_type = order_type;
    }
    public String getPayment_method() {
        return payment_method;
    }
    public void setPayment_method(String payment_method) {
        this.payment_method = payment_method;
    }
    public Integer getSubscription_flag() {
        return subscription_flag;
    }
    public void setSubscription_flag(Integer subscription_flag) {
        this.subscription_flag = subscription_flag;
    }
    public String getTransaction_id() {
        return transaction_id;
    }
    public void setTransaction_id(String transaction_id) {
        this.transaction_id = transaction_id;
    }
    public String getQuotation_reference() {
        return quotation_reference;
    }
    public void setQuotation_reference(String quotation_reference) {
        this.quotation_reference = quotation_reference;
    }
    public String getBundle_id() {
        return bundle_id;
    }
    public void setBundle_id(String bundle_id) {
        this.bundle_id = bundle_id;
    }
    public String getOffer_id() {
        return offer_id;
    }
    public void setOffer_id(String offer_id) {
        this.offer_id = offer_id;
    }
    public Integer getParent_offer_id() {
        return parent_offer_id;
    }
    public void setParent_offer_id(Integer parent_offer_id) {
        this.parent_offer_id = parent_offer_id;
    }
    public String getParent_trx_id() {
        return parent_trx_id;
    }
    public void setParent_trx_id(String parent_trx_id) {
        this.parent_trx_id = parent_trx_id;
    }
    public Integer getBulk_id() {
        return bulk_id;
    }
    public void setBulk_id(Integer bulk_id) {
        this.bulk_id = bulk_id;
    }
    public Integer getPrice() {
        return price;
    }
    public void setPrice(Integer price) {
        this.price = price;
    }
    public Integer getParent_price() {
        return parent_price;
    }
    public void setParent_price(Integer parent_price) {
        this.parent_price = parent_price;
    }
    public Integer getPenalty_remaining_period() {
        return penalty_remaining_period;
    }
    public void setPenalty_remaining_period(Integer penalty_remaining_period) {
        this.penalty_remaining_period = penalty_remaining_period;
    }
    public Integer getScheduler_scenario() {
        return scheduler_scenario;
    }
    public void setScheduler_scenario(Integer scheduler_scenario) {
        this.scheduler_scenario = scheduler_scenario;
    }
    public Integer getCorporate_id() {
        return corporate_id;
    }
    public void setCorporate_id(Integer corporate_id) {
        this.corporate_id = corporate_id;
    }
    public String getService_id_a() {
        return service_id_a;
    }
    public void setService_id_a(String service_id_a) {
        this.service_id_a = service_id_a;
    }
    public String getService_id_b() {
        return service_id_b;
    }
    public void setService_id_b(String service_id_b) {
        this.service_id_b = service_id_b;
    }
    public String getPurchase_mode() {
        return purchase_mode;
    }
    public void setPurchase_mode(String purchase_mode) {
        this.purchase_mode = purchase_mode;
    }
    public String getCallback_url() {
        return callback_url;
    }
    public void setCallback_url(String callback_url) {
        this.callback_url = callback_url;
    }

    


    
}
