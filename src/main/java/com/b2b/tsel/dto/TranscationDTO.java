package com.b2b.tsel.dto;

public class TranscationDTO {
    
    private String transaction_id;
    private String channel_transaction_id;
    private String channel;
    private String status_code;
    private String status_desc;

    public TranscationDTO(){

    }

    public TranscationDTO(String transaction_id, String channel_transaction_id, String channel, String status_code,
            String status_desc) {
        this.transaction_id = transaction_id;
        this.channel_transaction_id = channel_transaction_id;
        this.channel = channel;
        this.status_code = status_code;
        this.status_desc = status_desc;
    }
    public String getTransaction_id() {
        return transaction_id;
    }
    public void setTransaction_id(String transaction_id) {
        this.transaction_id = transaction_id;
    }
    public String getChannel_transaction_id() {
        return channel_transaction_id;
    }
    public void setChannel_transaction_id(String channel_transaction_id) {
        this.channel_transaction_id = channel_transaction_id;
    }
    public String getChannel() {
        return channel;
    }
    public void setChannel(String channel) {
        this.channel = channel;
    }
    public String getStatus_code() {
        return status_code;
    }
    public void setStatus_code(String status_code) {
        this.status_code = status_code;
    }
    public String getStatus_desc() {
        return status_desc;
    }
    public void setStatus_desc(String status_desc) {
        this.status_desc = status_desc;
    }

    


}
